<?php

if(! function_exists('display')){
   function display($str = null)
   {
       if($str == null){ return null; }
       return lang('English.'.$str);
   }
}