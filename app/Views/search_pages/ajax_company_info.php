<div class="table-responsive">
   <table class="table table-sm table-bordered table-striped">
         <caption style="caption-side: top !important;">Total Records</caption>
         <thead>
            <tr>
               <th scope="col">#</th>
               <th scope="col"><?=display('UID')?></th>
               <th scope="col"><?=display('Type')?></th>
               <th scope="col"><?=display('Business Name')?></th>
               <th scope="col"><?=display('Entry of')?></th>
               <th scope="col"><?=display('Share Capital')?></th>
               <th scope="col"><?=display('C/O')?></th>
               <th scope="col"><?=display('Street')?></th>
               <th scope="col"><?=display('Zip')?></th>
               <th scope="col"><?=display('City')?></th>
               <th scope="col"><?=display('Name')?></th>
               <th scope="col"><?=display('Surname')?></th>
            </tr>
         </thead>
         <tbody>
            <?php
            $sl = 1;
            foreach ($company_info as $c) {
            ?>
            <tr>
               <th scope="row"><?=$sl?></th>
               <td><?=$c->uid?></td>
               <td><?=$c->type?></td>
               <td><?=$c->business_name?></td>
               <td><?=$c->entry_of?></td>
               <td></td>
               <td></td>
               <td><?=$c->street?></td>
               <td><?=$c->zip_code?></td>
               <td><?=$c->place?></td>
               <td></td>
               <td></td>
            </tr>
            <?php $sl++; ?>
            <?php } ?>
         </tbody>
   </table>
</div>
<?=$pagination?>