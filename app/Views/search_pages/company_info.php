<!-- <style>
   /* //----------  responsive breakpoints
   //------------------------------------------------------------------------------ */
   @mixin breakpoint ($value) {
      @if $value == 'phone' {
         @media only screen and (min-width: 120px) and (max-width: 767px) { @content; }
      } @else if $value == 'tablet' {
         @media only screen and (min-width: 768px) and (max-width: 1024px) { @content; }
      } @else if $value == 'touch' {
         @media only screen and (min-width: 120px) and (max-width: 1024px) { @content; }
      } @else if $value == 'desktop' {
         @media only screen and (min-width: 1025px) { @content; }
      } @else {
         @media only screen and (max-width: $value) { @content; }
      }
   }

   /* Colors */
   $white: #fff;
   $black: #000;
   $grey: #595959;
   $grey-dark: #2b2b2b;
   $grey-light: #eee;
   $green: #86c023;
   $blue: #017ac7;

   /* Transition */
   $duration: 400ms;
   $easing: ease;

   /*  reset */
   /* ------------------------------------------------------------------------------ */
   htm,
   body {
   font-family: sans-serif;
   }

   a {
   text-decoration: none;
   }

   /* pagination
   ------------------------------------------------------------------------------ */
   .pagination-wrapper {
      text-align: center;
      margin: 40px 0;
   }

   .pagination {
      display: inline-block;
      height: 70px;
      margin-top: 70px;
      padding: 0 25px;
      border-radius: 35px;
      background-color: $grey-light;

      @include breakpoint(1199px) {
         height: 50px;
         margin-top: 50px;
         padding: 0 10px;
         border-radius: 25px;
      }
   }

   .page-numbers {
      display: block;
      padding: 0 25px;
      float: left;
      transition: $duration $easing;
      color: $grey;
      font-size: 20px;
      letter-spacing: 0.1em;
      line-height: 70px;

      &:hover,
      &.current {
         background-color: $green;
         color: $white;
      }

      &.prev:hover,
      &.next:hover {
         background-color: transparent;
      color: $green;
      }

      @include breakpoint(1199px) {
         padding: 0 15px;
         font-size: 16px;
         line-height: 50px;
      }

      @include breakpoint(touch) {
         padding: 0 14px;
         display: none;

         &:nth-of-type(2) {
            position: relative;
            padding-right: 50px;

            &::after {
               content: '...';
               position: absolute;
               font-size: 25px;
               top: 0;
               left: 45px;
            }
         }

         &:nth-child(-n+3),
         &:nth-last-child(-n+3) {
            display: block;
         }

         &:nth-last-child(-n+4) {
            padding-right: 14px;

            &::after {
               content: none;
            }
         }
      }
   }
</style> -->
<div class="card">
   <div class="card-body">
      <div class="repeater-default m-t-10">
         <div data-repeater-list="">
               <div data-repeater-item="">
                  <?=form_open('', 'class="search_form"');?>
                     <div class="form-row">
                           <div class="form-group col-md-3">
                              <label for="name"><?=display('Zip Code')?></label>
                              <input type="text" class="form-control" name="zip_code" id="zip_code" placeholder="<?=display('Zip Code')?>">
                           </div>
                           <div class="form-group col-md-3">
                              <label for="email"><?=display('Name of Company')?></label>
                              <input type="text" class="form-control" id="company_name" name="company_name" placeholder="<?=display('Name of Company')?>">
                           </div>
                           <div class="form-group col-md-3">
                              <label for="type"><?=display('Type of Company')?></label>
                              <input type="text" class="form-control" id="type" name="type" placeholder="<?=display('Type of Company')?>">
                           </div>
                           <div class="form-group col-md-3">
                              <label for="purpose"><?=display('Purpose')?></label>
                              <textarea class="form-control" id="purpose" name="purpose" rows="1" placeholder="<?=display('Purpose')?>"></textarea>
                           </div>
                     </div>
                     <div class="form-row"></div>
                     <div class="form-row">
                        <div class="form-group col-md-12 text-right">
                           <button class="btn btn-success waves-effect waves-light" type="submit" id="btn_search"><?=display('Search')?>
                           </button>
                           <button data-repeater-delete="" class="btn btn-danger waves-effect waves-light m-l-10" type="button" id="clear_search"><?=display('Clear Form')?>
                           </button>
                        </div>
                     </div>
                  </form>
                  <!-- <hr class="m-t-5 m-b-5"> -->
               </div>
         </div>
      </div>
      <!-- <h4 class="card-title">Table With Caption</h4> -->
      <div id="company_list">
      </div>
      <!-- <nav aria-label="Page navigation example" class="m-t-40">
         <ul class="pagination">
               <li class="page-item disabled">
                  <a class="page-link" href="javascript:void(0)" tabindex="-1">Previous</a>
               </li>
               <li class="page-item"><a class="page-link" href="javascript:void(0)">1</a></li>
               <li class="page-item"><a class="page-link" href="javascript:void(0)">2</a></li>
               <li class="page-item"><a class="page-link" href="javascript:void(0)">3</a></li>
               <li class="page-item">
                  <a class="page-link" href="javascript:void(0)">Next</a>
               </li>
         </ul>
      </nav> -->
   </div>
</div>