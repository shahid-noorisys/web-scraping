<style>
   /* //---------- responsive breakpoints //------------------------------------------------------------------------------ */
   /* Colors */
   /* Transition */
   /* reset */
   /* ------------------------------------------------------------------------------ */
   body {
      font-family: sans-serif;
   }
   a {
      text-decoration: none;
   }
   /* pagination ------------------------------------------------------------------------------ */
   .pagination-wrapper {
      text-align: center;
      margin: 10px 0;
   }
   .pagination {
      display: inline-block;
      height: 30px;
      margin-top: 8px;
      /* padding: 0 12px; */
      border-radius: 25px;
      background-color: #eee;
   }
   @media only screen and (max-width: 1199px) {
      .pagination {
         height: 20px;
         margin-top: 8px;
         /* padding: 0 12px; */
         border-radius: 15px;
      }
   }
   .page-numbers {
      display: block;
      padding: 0 12px;
      float: left;
      transition: 400ms ease;
      color: #595959;
      font-size: 14px;
      letter-spacing: 0.1em;
      line-height: 30px;
   }
   .page-numbers:hover, .page-numbers.current {
      background-color: #3587d8;
      color: #fff;
   }
   .page-numbers.prev:hover, .page-numbers.next:hover {
      background-color: transparent;
      color: #3587d8;
   }
   @media only screen and (max-width: 1199px) {
      .page-numbers {
         padding: 0 12px;
         font-size: 14px;
         line-height: 20px;
      }
   }
   @media only screen and (min-width: 120px) and (max-width: 1024px) {
      .page-numbers {
         padding: 0 12px;
         display: none;
      }
      .page-numbers:nth-of-type(2) {
         position: relative;
         padding-right: 8px;
      }
      .page-numbers:nth-of-type(2)::after {
         content: '...';
         position: absolute;
         font-size: 14px;
         top: 0;
         left: 45px;
      }
      .page-numbers:nth-child(-n+3), .page-numbers:nth-last-child(-n+3) {
         display: block;
      }
      .page-numbers:nth-last-child(-n+4) {
         padding-right: 8px;
      }
      .page-numbers:nth-last-child(-n+4)::after {
         content: none;
      }
   }
   
</style>
<?php

/**
 * @var \CodeIgniter\Pager\PagerRenderer $pager
 */
$pager->setSurroundCount(2);
?>

<?php if(COUNT($pager->links()) > 1): ?>
<div class="pagination-wrapper">
   <div class="pagination">
      <?php if($pager->hasPreviousPage()): ?>
      <a class="prev page-numbers" href="<?=($pager->hasPreviousPage())?$pager->getPreviousPage():'#'?>"><?= lang('Pager.previous') ?></a>
      <?php else: ?>
         <span class="prev page-numbers" style="cursor: no-drop;"><?=lang('Pager.previous')?></span>
      <?php endif; ?>

      <?php if ($pager->hasPreviousPage()) : ?>
         <a class="first page-numbers" href="<?= $pager->getFirst() ?>"><?= lang('Pager.first') ?></a>
      <?php endif; ?>

		<?php foreach ($pager->links() as $link) : ?>
         <?php if($link['active']): ?>
            <span aria-current="page" class="page-numbers current"><?=$link['title']?></span>
         <?php else: ?>
            <a class="page-numbers" href="<?= $link['uri'] ?>"><?= $link['title'] ?></a>
         <?php endif; ?>
		<?php endforeach ?>

      <?php if ($pager->hasNextPage()) : ?>
         <a class="first page-numbers" href="<?= $pager->getLast() ?>"><?= lang('Pager.last') ?></a>
      <?php endif; ?>
      <?php if($pager->hasNextPage()): ?>
      <a class="next page-numbers" href="<?= ($pager->hasNextPage())?$pager->getNextPage():'#' ?>"><?= lang('Pager.next') ?></a>
      <?php else: ?>
         <span class="next page-numbers" style="cursor: no-drop;"><?=lang('Pager.next')?></span>
      <?php endif; ?>

   </div>
</div>
<?php endif; ?>