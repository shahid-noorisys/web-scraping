<aside class="left-sidebar">
   <!-- Sidebar scroll-->
   <div class="scroll-sidebar">
         <!-- Sidebar navigation-->
         <nav class="sidebar-nav">
            <ul id="sidebarnav">
               <!-- <li class="nav-small-cap"><i class="mdi mdi-dots-horizontal"></i> <span class="hide-menu">Personal</span></li> -->
               <li class="sidebar-item"> <a class="sidebar-link" href="<?=base_url()?>"><i class="icon-Car-Wheel"></i><span class="hide-menu"><?=display('Dashboard')?> </span></a>
               </li>
               <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="icon-Navigation-LeftWindow"></i><span class="hide-menu"><?=display('Search')?> </span></a>
                  <ul aria-expanded="false" class="collapse  first-level">
                     <li class="sidebar-item"><a href="<?=base_url('search-page')?>" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu"> <?=display('Company Information')?> </span></a></li>
                     <!-- <li class="sidebar-item"><a href="index2.html" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu"> Dashboard 2 </span></a></li>
                     <li class="sidebar-item"><a href="index3.html" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu"> Dashboard 3 </span></a></li> -->
                  </ul>
               </li>
               <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="icon-Increase-Inedit"></i><span class="hide-menu"><?=display('Scraping')?></span></a>
                     <ul aria-expanded="false" class="collapse first-level">
                        <li class="sidebar-item"><a href="<?=base_url('page-start')?>" class="sidebar-link"><i class="mdi mdi-octagram"></i><span class="hide-menu"> <?=display('Start')?></span></a></li>
                        <!-- <li class="sidebar-item"><a href="#" class="sidebar-link"><i class="mdi mdi-octagram"></i><span class="hide-menu"> Name</span></a></li> -->
                        <!-- <li class="sidebar-item"> <a class="has-arrow sidebar-link" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-playlist-plus"></i> <span class="hide-menu">Menu 1.3</span></a>
                           <ul aria-expanded="false" class="collapse second-level">
                                 <li class="sidebar-item"><a href="javascript:void(0)" class="sidebar-link"><i class="mdi mdi-octagram"></i><span class="hide-menu"> item 1.3.1</span></a></li>
                                 <li class="sidebar-item"><a href="javascript:void(0)" class="sidebar-link"><i class="mdi mdi-octagram"></i><span class="hide-menu"> item 1.3.2</span></a></li>
                                 <li class="sidebar-item"><a href="javascript:void(0)" class="sidebar-link"><i class="mdi mdi-octagram"></i><span class="hide-menu"> item 1.3.3</span></a></li>
                                 <li class="sidebar-item"><a href="javascript:void(0)" class="sidebar-link"><i class="mdi mdi-octagram"></i><span class="hide-menu"> item 1.3.4</span></a></li>
                           </ul>
                        </li> -->
                        <!-- <li class="sidebar-item"><a href="javascript:void(0)" class="sidebar-link"><i class="mdi mdi-playlist-check"></i><span class="hide-menu"> item 1.4</span></a></li> -->
                     </ul>
               </li>
            </ul>
         </nav>
         <!-- End Sidebar navigation -->
   </div>
   <!-- End Sidebar scroll-->
</aside>