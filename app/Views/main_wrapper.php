<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?=base_url('public/assets/images/logo/favicon.png')?>">
    <title><?=display('Search Company Informations')?></title>
    <!-- Custom CSS -->
    <link href="<?=base_url('public/assets/css/style.css')?>" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <?=$this->include('partials/header')?>
        <!-- End Topbar header -->
        <?=$this->include('partials/top_menu_bar')?>
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <?=$this->include('partials/bread_crumb');?>
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <?php 
                if(!empty($content)){
                    echo $content;
                }
                ?>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
           <?=$this->include('partials/footer');?>
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="<?=base_url('public/assets/js/jquery.min.js')?>"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?=base_url('public/assets/js/popper.min.js')?>"></script>
    <script src="<?=base_url('public/assets/js/bootstrap.min.js')?>"></script>
    <!-- apps -->
    <script src="<?=base_url('public/assets/js/app.js')?>"></script>
    <script src="<?=base_url('public/assets/js/app.init.horizontal-fullwidth.js')?>"></script>
    <script src="<?=base_url('public/assets/js/app-style-switcher.horizontal.js')?>"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?=base_url('public/assets/js/perfect-scrollbar.jquery.min.js')?>"></script>
    <script src="<?=base_url('public/assets/js/sparkline.js')?>"></script>
    <!--Wave Effects -->
    <script src="<?=base_url('public/assets/js/waves.js')?>"></script>
    <!--Menu sidebar -->
    <script src="<?=base_url('public/assets/js/sidebarmenu.js')?>"></script>
    <!--Custom JavaScript -->
    <script src="<?=base_url('public/assets/js/custom.js')?>"></script>

    <script>
    $(document).ready(function () {
        //-------- For Company Search pagination--------//
        $(".search_form").submit(function (e) { 
            e.preventDefault();
        });
        ajaxCompanyInfo(page_url = false);
        $(document).on('click','.pagination a', function(e){
            e.preventDefault();
            var page_url = $(this).attr('href');
            // alert(page_url);
            ajaxCompanyInfo(page_url);
        });
        //-------- For Company Search pagination--------//
        //-------- For Company Search Filter--------//
        $(document).on('click',"#btn_quote_filter", function(e){
            e.preventDefault();
            ajaxCompanyInfo(page_url = false);
        });
        $(document).on('click',"#quote_btn_clear_filter", function(e){
            e.preventDefault();
            $("#quote_no").val("");
            $("#quote_status").val("");
            ajaxCompanyInfo(page_url = false);
        });
        //-------- For Company Search Filter--------//
    });
    function ajaxCompanyInfo(page_url)
    {
        // var quotation_no = $("#quote_no").val();
        // var statu = $("#quote_status").val();
        var base_url = "<?=base_url('quick-search')?>";
        if(page_url){
            base_url = page_url;
        }
        $.ajax({
            type: "post",
            url: base_url,
            headers: {'X-Requested-With': 'XMLHttpRequest'},
            data: {'<?=csrf_token()?>': '<?=csrf_hash()?>'},//,quotation_no: quotation_no,status: status},
            dataType: "json",
            success: function (response) {
                if(response.status == 'success') {
                $("#company_list").html(response.html);
                } else {
                $("#company_list").html(response.html);
                }
            }
        });
    }
    </script>
</body>

</html>