<div class="row">
    <div class="col-md-6">
        <form action="<?=base_url('html-parse')?>" method="post">
            <label for="" class="m-l-10"><?=display('Select rows to parse')?></label>
            <div class="col-md-8">
                <select name="limit" id="input" class="form-control">
                    <option value="0">-- <?=display('Select One')?> --</option>
                    <option value="5">5</option>
                    <option value="10">10</option>
                    <option value="20">20</option>
                    <option value="50">50</option>
                    <option value="80">80</option>
                    <option value="100">100</option>
                    <option value="500">500</option>
                </select>
            </div>
            <br>
            <button type="submit" class="btn btn-success m-l-10"><?=display('Save')?></button>
        </form>
    </div>
    <div class="col-md-6">
    <a class="btn btn-warning d-none" href="<?=base_url('html-get')?>" role="button"><?=display('Start Fetching')?></a>
    </div>
</div>