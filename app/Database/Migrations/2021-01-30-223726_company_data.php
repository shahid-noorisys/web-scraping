<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CompanyData extends Migration
{
	public function up()
	{
		$fields = [
			'id' => [
				'type' => 'BIGINT',
				'constraint' => 20,
				'unsigned' => true,
				'auto_increment' => true
			],
			'uid' => [
				'type' => 'VARCHAR',
				'constraint' => '255'
			],
			'html_data' => [
				'type' => 'BLOB',
				'comment' => 'store html data from web'
			],
			'is_fetched' => [
				'type' => 'TINYINT',
				'constraint' => '1',
				'default' => 0,
			],
			'created_date' => [
				'type' => 'VARCHAR',
				'constraint' => '50'
			],
		];
		$this->forge->addField($fields);
		$this->forge->addKey('id', true);
		$this->forge->createTable('company_html_data');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->dropTable('company_html_data');
	}
}
