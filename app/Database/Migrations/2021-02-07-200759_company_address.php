<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CompanyAddress extends Migration
{
	public function up()
	{
		$fields = [
			'id' => [
				'type' => 'BIGINT',
				'constraint' => 20,
				'unsigned' => true,
				'auto_increment' => true
			],
			'c_id' => [
				'type' => 'BIGINT',
				'constraint' => 20,
				'comment' => 'company info table ID',
			],
			'uid' => [
				'type' => 'VARCHAR',
				'constraint' => '255'
			],
			'in' => [
				'type' => 'VARCHAR',
				'constraint' => 50,
			],
			'ca' => [
				'type' => 'VARCHAR',
				'constraint' => 50,
			],
			'address' => [
				'type' => 'VARCHAR',
				'constraint' => 255,
			],
			'status' => [
				'type' => 'TINYINT',
				'constraint' => '1',
				'default' => 1,
				'comment' => '0:Inactive, 1:Active'
			],
			'deleted' => [
				'type' => 'TINYINT',
				'constraint' => '1',
				'default' => 0,
				'comment' => '0:not deleted, 1:deleted'
			],
			'updated_date' => [
				'type' => 'VARCHAR',
				'constraint' => 50
			],
			'created_date' => [
				'type' => 'VARCHAR',
				'constraint' => 50
			],
		];
		$this->forge->addField($fields);
		$this->forge->addKey('id', true);
		$this->forge->createTable('company_addresses');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->dropTable('company_addresses');
	}
}
