<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CompanyInfo extends Migration
{
	public function up()
	{
		$fields = [
			'id' => [
				'type' => 'BIGINT',
				'constraint' => 20,
				'unsigned' => true,
				'auto_increment' => true
			],
			'uid' => [
				'type' => 'VARCHAR',
				'constraint' => '255'
			],
			'business_name' => [
				'type' => 'VARCHAR',
				'constraint' => 255,
			],
			'type' => [
				'type' => 'VARCHAR',
				'constraint' => 255,
			],
			'ca' => [
				'type' => 'VARCHAR',
				'constraint' => 50,
			],
			'legal_seat' => [
				'type' => 'VARCHAR',
				'constraint' => 255,
			],
			'full_address' => [
				'type' => 'VARCHAR',
				'constraint' => 255,
			],
			'street' => [
				'type' => 'VARCHAR',
				'constraint' => 100,
			],
			'zip_code' => [
				'type' => 'VARCHAR',
				'constraint' => 100,
			],
			'place' => [
				'type' => 'VARCHAR',
				'constraint' => 100,
			],
			'entry_of' => [
				'type' => 'VARCHAR',
				'constraint' => 100,
			],
			'status' => [
				'type' => 'TINYINT',
				'constraint' => '1',
				'default' => 1,
				'comment' => '0:Inactive, 1:Active'
			],
			'deleted' => [
				'type' => 'TINYINT',
				'constraint' => '1',
				'default' => 0,
				'comment' => '0:not deleted, 1:deleted'
			],
			'updated_date' => [
				'type' => 'VARCHAR',
				'constraint' => 50
			],
			'created_date' => [
				'type' => 'VARCHAR',
				'constraint' => 50
			],
		];
		$this->forge->addField($fields);
		$this->forge->addKey('id', true);
		$this->forge->createTable('company_info');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->dropTable('company_info');
	}
}
