<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class FetchUrl extends Migration
{
	public function up()
	{
		$fields = [
			'id' => [
				'type' => 'BIGINT',
				'constraint' => 20,
				'unsigned' => true,
				'auto_increment' => true
			],
			'company_uid' => [
				'type' => 'VARCHAR',
				'constraint' => '255'
			],
			'company_url' => [
				'type' => 'VARCHAR',
				'constraint' => '255'
			],
			'region' => [
				'type' => 'VARCHAR',
				'constraint' => '100'
			],
			'is_read' => [
				'type' => 'TINYINT',
				'constraint' => '1'
			],
			'created_date' => [
				'type' => 'VARCHAR',
				'constraint' => '50'
			],
		];
		$this->forge->addField($fields);
		$this->forge->addKey('id', true);
		$this->forge->createTable('fetch_url');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->dropTable('fetch_url');
	}
}
