<?php
namespace App\Controllers;

use App\Models\CustomModel;


class CompanyInfo extends BaseController
{
   protected $companyModel = null;
   
   public function __construct()
   {
      $this->companyModel = new CustomModel();
   }
   
   public function companySearch()
   {
      $data['title'] = display("Search Page");
      $data['bread_crumb'] = display('Company Information');
      $data['content'] = view('search_pages/company_info',$data);
      return view('main_wrapper',$data);
   }

   public function ajaxCompanySearch()
   {
      $response = array();
      if($this->request->isAJAX() AND $this->request->getMethod() == 'post')
      {
         // $quotation_no = $this->request->getPost('quotation_no');
         // $status = $this->request->getPost('status');
         $pageNo = $this->request->getGet('page');
         $perPage = env('pager.perPage');
         if($pageNo == '' || $pageNo == null){ $pageNo = 1;}
         // for filter
         // if(! empty(trim($quotation_no))){
         //    $this->quoteModel->like('q_no',$quotation_no);
         //    $this->quoteFilterModel->like('q_no',$quotation_no);
         // }
         // if(! empty(trim($status))){
         //       $this->quoteModel->where('status',$status);
         //       $this->quoteFilterModel->where('status',$status);
         // }
         
         // echo json_encode($data['quotations']);exit;
         // $pager = $this->quoteModel->pager;
         // echo json_encode($response);


         $offset = $pageNo * $perPage;
         $query_data['limit'] = $perPage;
         $query_data['offset'] = $offset;
         $data['company_info'] = $this->companyModel->getCompanyDetails($query_data);
         $data['total'] = $total = $this->companyModel->getCompanyDetailsTotal($query_data);
         // $total = COUNT($data['company_info']);
         // $pager = $this->companyModel->pager;
         $pager = service('pager');
         $data['pagination'] = $pager->makeLinks($pageNo,$perPage,$total,'custom_pagination');
         $html = view('search_pages/ajax_company_info',$data);
         $response = ['status' => 'success','html' => $html, 'page_no' => $pageNo];
      } else {
         $response = ['status'=>'failed','html'=>'Request is not supported','page_no' => ''];
      }
      echo json_encode($response);
   }
}