<?php namespace App\Controllers;

use Symfony\Component\DomCrawler\Crawler;

class Home extends BaseController
{
	public function index()
	{
		$data['title'] = display('Dashboard');
		$data['bread_crumb'] = display('Home');
		$data['content'] = view('home',$data);
		return view('main_wrapper',$data);
	}

	public function starterPage()
	{
		$data['title'] = display('Store Data');
		$data['bread_crumb'] = display('Home');
		$data['content'] = view('start',$data);
		return view('main_wrapper',$data);
	}

	public function fetchHtml()
	{
		$db_table = $this->db->table('fetch_url');
		$res = $db_table->where('is_read',0)->get()->getRow();
		if(!empty($res)){
			return redirect()->to($res->company_url);
		}
		$data['title'] = display('Dashboard');
		$data['bread_crumb'] = display('Home');
		$data['content'] = view('start',$data);
		return view('main_wrapper',$data);
		// $client = new Client();
		// $url = 'https://ag.chregister.ch/cr-portal/auszug/auszug.xhtml?uid=CHE-115.791.109';
		// $crawler = $client->request('GET', $url);
		// Get the latest post in this category and display the titles
		// $crawler->filter('html')->each(function ($node) {
			// echo $node->html()."<br>";
		// 	echo strip_tags($node->html(), '<br>');
		// });
		// print_r($crawler->filter('#navbar')->children());
	}

	public function insertHtml()
	{
		if($this->request->isAJAX())
		{
			$html = $this->request->getPost('fetched_data');
			$uid = $this->request->getPost('uid');
			$url_uid = $this->request->getPost('url_uid');
			$company_html_data = [
				'uid' => $url_uid,
				'html_data' => $html,
				'created_date' => date('Y-m-d H:i:s')
			];
			// echo json_encode($company_html_data);
			// echo "hellooo";exit;
			$co_table = $this->db->table('company_html_data');
			if($co_table->insert($company_html_data))
			{
				$db_table = $this->db->table('fetch_url');
				if(!empty($uid)){ $data = ['is_read'=>1];}
				else { $data = ['is_read'=>2]; }
				$db_table->where('company_uid',$url_uid)->update($data);
				// echo json_encode($db_table->where('is_read',0)->get()->getRow());
				$res = $db_table->where('is_read',0)->get()->getRow();
				echo json_encode(['status' => 'success', 'url' => $res->company_url]);exit;
			} else {
				echo json_encode(['status' => 'failed', 'url' => null]);exit;
			}
		} else {
			echo json_encode(['status' => 'failed', 'url' => null]);exit;
		}
	}

	public function parseHtml()
	{
		set_time_limit(0);
		if($this->request->getMethod() == 'post' AND $this->request->getPost('limit') != '0')
		{
			$limit = $this->request->getPost('limit');
			$table = $this->db->table('company_html_data');
			$result = $table->where('is_fetched',0)->limit($limit)->get()->getResult();
			// $result = $table->where('id',1624)->limit(1)->get()->getResult();
			// echo json_encode(COUNT($result));exit;
			// echo strchr('In Ca Purpose','Purpose');
			if(!empty($result))
			{
				foreach ($result as $res)
				{
					$dom = new Crawler($res->html_data);
					
					$title = $dom->filter('#Titel p span')->each(function (Crawler $node,$i){ return $node->html(); });
					$title_p = $dom->filter('#Titel p')->each(function (Crawler $node,$i){
						return $node->html();
					});
					if(isset($title[2]) AND !empty($title[2]))
					{
						$tbody_1 = $dom->filter('.table tbody')->eq(0)->filter('tr')->each(function (Crawler $node,$i){
							return $node->filter('td')->each(function (Crawler $n,$c){ return $n->html(); });
						});
						$tbody_2 = $dom->filter('.table tbody')->eq(1)->filter('tr')->each(function (Crawler $node,$i){
							return $node->filter('td')->each(function (Crawler $n,$c){ return strip_tags($n->html()); });
						});
						// $leagal_seat = trim($tbody_2[0][1]);
						// $leagal_seat = substr($leagal_seat,-(strrpos($leagal_seat,' ',-1)+1));
						$thead = $dom->filter('.table thead')->each(function (Crawler $node,$c){ return $node->text(); });
						// echo json_encode(COUNT($tbody_purpose));
						// echo "<pre>";
						// print_r($thead) ;
						// echo "</pre>";

						$tbody_3 = $dom->filter('.table tbody')->eq(2)->filter('tr')->each(function (Crawler $node,$i){
							return $node->filter('td')->each(function (Crawler $n,$c){ return $n->html(); });
						});
						if(strchr($thead[3],'Company address')){
							$tbody_4 = $dom->filter('.table tbody')->eq(3)->filter('tr')->each(function (Crawler $node,$i){
								return $node->filter('td')->each(function (Crawler $n,$c){ return $n->html(); });
							});
						} else {
							$tbody_4 = $dom->filter('.table tbody')->eq(4)->filter('tr')->each(function (Crawler $node,$i){
								return $node->filter('td')->each(function (Crawler $n,$c){ return $n->html(); });
							});
						}
						
						$count = COUNT($tbody_4)-1;
						$place = strip_tags($title_p[2]);
						$full_address = strip_tags((str_replace('<br>',' ',$tbody_4[$count][2])));
						// print_r($count);
						// print_r($tbody_4);exit;
						$number_str = filter_var($full_address, FILTER_SANITIZE_NUMBER_INT);
						$zip_code = '';
						if(!empty($number_str)){ $zip_code = substr($number_str,-4);}
						// echo $zip_code;exit;
						$street = '';
						if(!empty($zip_code)){
						$street = substr($full_address, 0, strpos($full_address, $zip_code));
						}
						$company_info = [
							'uid' => trim($title[2]),
							'business_name' => trim($title[0]),
							'entry_of' => trim($title[4]),
							'type' => trim($title_p[1]),
							'ca' => trim(strip_tags($tbody_1[0][1])),
							'legal_seat' => trim($place),
							'full_address' => trim(str_replace('  ',' ',$full_address)),
							'street' => trim($street),
							'zip_code' => $zip_code,
							'place' => trim($place),
							'updated_date' => date('Y-m-d H:i:s'),
							'created_date' => date('Y-m-d H:i:s'),
						];
						// print_r($company_info);exit;
						
						$company_table = $this->db->table('company_info');
						$company_table->insert($company_info);
						$c_id = $this->db->insertID();
						// $c_id = 1;
						$purpose_array = array();
						$capital_array = array();
						$shares_array = array();
						$share_capital_array = array();
						$com_address_array = array();
						$other_address_array = array();
						$remarks_array = array();
						$acts_array = array();
						$qualified_array = array();
						$accessory_payment_array = array();
						$journal_array = array();
						$personal_array = array();
						$business_array = array();
						$pubilcation_array = array();
						$branch_office_array = array();
						foreach ($thead as $key => $value) {
							$tbody = $dom->filter('.table tbody')->eq($key)->filter('tr')->each(function (Crawler $node,$i){
								return $node->filter('td')->each(function (Crawler $n,$c){ return $n->html(); });
							});
							if(strchr($value,'Business') AND strchr($value,'name'))
							{
								if(!empty($tbody))
								{
									foreach ($tbody as $key => $v) {
										$b = (isset($v[2]))?trim(strip_tags($v[2])):'';
										// preg_match('/\s[a-zA-z]*\s.*/D',$b,$name);
										$name = substr($b, strpos($b, ":") + 1);
										$business_array[] = [
											'uid' => trim($title[2]),
											'c_id' => $c_id,
											'in' => (isset($v[0]))?trim(strip_tags($v[0])):'',
											'ca' => (isset($v[1]))?trim(strip_tags($v[1])):'',
											'business_name' => trim($name),
											'updated_date' => date('Y-m-d H:i:s'),
											'created_date' => date('Y-m-d H:i:s'),
										];
									}
								}
							}
							if(strchr($value,'Purpose') AND strchr($value,'Ca'))
							{
								if(!empty($tbody))
								{
									foreach ($tbody as $key => $v) {
										$purpose_array[] = [
											'uid' => trim($title[2]),
											'c_id' => $c_id,
											'in' => (isset($v[0]))?trim(strip_tags($v[0])):'',
											'ca' => (isset($v[1]))?trim(strip_tags($v[1])):'',
											'purpose' => (isset($v[2]))?trim(strip_tags($v[2])):'',
											'updated_date' => date('Y-m-d H:i:s'),
											'created_date' => date('Y-m-d H:i:s'),
										];
									}
								}
							}
							if(strchr($value,'Ref') AND strchr($value,'Capital'))
							{
								if(!empty($tbody))
								{
									foreach ($tbody as $key => $v) {
										$capital_array[] = [
											'uid' => trim($title[2]),
											'c_id' => $c_id,
											'ref' => (isset($v[0]))?trim(strip_tags($v[0])):'',
											'capital' => (isset($v[1]))?trim(strip_tags($v[1])):'',
											'updated_date' => date('Y-m-d H:i:s'),
											'created_date' => date('Y-m-d H:i:s'),
											// 'ca' => trim(strip_tags($v[2])),
											// 'shares' => trim(strip_tags($v[3])),
											// 'partner' => trim(strip_tags($v[4])),
										];
									}
								}
							}
							if(strchr($value,'Shares') AND strchr($value,'Partner'))
							{
								if(!empty($tbody))
								{
									foreach ($tbody as $key => $v) {
										$shares_array[] = [
											'uid' => trim($title[2]),
											'c_id' => $c_id,
											'in' => (isset($v[0]))?trim(strip_tags($v[0])):'',
											'mo' => (isset($v[1]))?trim(strip_tags($v[1])):'',
											'ca' => (isset($v[2]))?trim(strip_tags($v[2])):'',
											'shares' => (isset($v[3]))?trim(strip_tags($v[3])):'',
											'partner' => (isset($v[4]))?trim(strip_tags($v[4])):'',
											'updated_date' => date('Y-m-d H:i:s'),
											'created_date' => date('Y-m-d H:i:s'),
										];
									}
								}
							}
							if(strchr($value,'Share capital') AND strchr($value,'Paid in'))
							{
								if(!empty($tbody))
								{
									foreach ($tbody as $key => $v) {
										$share_capital_array[] = [
											'uid' => trim($title[2]),
											'c_id' => $c_id,
											'in' => (isset($v[0]))?trim(strip_tags($v[0])):'',
											'mo' => (isset($v[1]))?trim(strip_tags($v[1])):'',
											'ca' => (isset($v[2]))?trim(strip_tags($v[2])):'',
											'share_capital' => (isset($v[3]))?trim(strip_tags($v[3])):'',
											'paid_in' => (isset($v[4]))?trim(strip_tags($v[4])):'',
											'shares' => (isset($v[5]))?trim(strip_tags($v[5])):'',
											'updated_date' => date('Y-m-d H:i:s'),
											'created_date' => date('Y-m-d H:i:s'),
										];
									}
								}
							}
							if(strchr($value,'Company') AND strchr($value,'address'))
							{
								if(!empty($tbody))
								{
									foreach ($tbody as $key => $v) {
										$com_address_array[] = [
											'uid' => trim($title[2]),
											'c_id' => $c_id,
											'in' => (isset($v[0]))?trim(strip_tags($v[0])):'',
											'ca' => (isset($v[1]))?trim(strip_tags($v[1])):'',
											'address' => (isset($v[2]))?trim(strip_tags($v[2])):'',
											'updated_date' => date('Y-m-d H:i:s'),
											'created_date' => date('Y-m-d H:i:s'),
										];
									}
								}
							}
							if(strchr($value,'Other') AND strchr($value,'addresses'))
							{
								if(!empty($tbody))
								{
									foreach ($tbody as $key => $v) {
										$other_address_array[] = [
											'uid' => trim($title[2]),
											'c_id' => $c_id,
											'in' => (isset($v[0]))?trim(strip_tags($v[0])):'',
											'ca' => (isset($v[1]))?trim(strip_tags($v[1])):'',
											'address' => (isset($v[2]))?trim(strip_tags($v[2])):'',
											'updated_date' => date('Y-m-d H:i:s'),
											'created_date' => date('Y-m-d H:i:s'),
										];
									}
								}
							}
							if(strchr($value,'Ca') AND strchr($value,'Remarks'))
							{
								if(!empty($tbody))
								{
									foreach ($tbody as $key => $v) {
										$remarks_array[] = [
											'uid' => trim($title[2]),
											'c_id' => $c_id,
											'in' => (isset($v[0]))?trim(strip_tags($v[0])):'',
											'ca' => (isset($v[1]))?trim(strip_tags($v[1])):'',
											'remarks' => (isset($v[2]))?trim(strip_tags($v[2])):'',
											'updated_date' => date('Y-m-d H:i:s'),
											'created_date' => date('Y-m-d H:i:s'),
										];
									}
								}
							}
							// Date of acts array
							if(strchr($value,'Date of the acts'))
							{
								if(!empty($tbody))
								{
									foreach ($tbody as $key => $v) {
										$acts_array[] = [
											'uid' => trim($title[2]),
											'c_id' => $c_id,
											'ref' => (isset($v[0]))?trim(strip_tags($v[0])):'',
											'act_date' => (isset($v[1]))?trim(strip_tags($v[1])):'',
											'updated_date' => date('Y-m-d H:i:s'),
											'created_date' => date('Y-m-d H:i:s'),
										];
									}
								}
							}
							// Qualified facts
							if(strchr($value,'Ca') AND strchr($value,'Qualified') AND strchr($value,'facts'))
							{
								if(!empty($tbody))
								{
									foreach ($tbody as $key => $v) {
										$qualified_array[] = [
											'uid' => trim($title[2]),
											'c_id' => $c_id,
											'in' => (isset($v[0]))?trim(strip_tags($v[0])):'',
											'ca' => (isset($v[1]))?trim(strip_tags($v[1])):'',
											'facts' => (isset($v[2]))?trim(strip_tags($v[2])):'',
											'updated_date' => date('Y-m-d H:i:s'),
											'created_date' => date('Y-m-d H:i:s'),
										];
									}
								}
							}
							// publication array
							if(strchr($value,'Ref') AND strchr($value,'Official publication'))
							{
								if(!empty($tbody))
								{
									foreach ($tbody as $key => $v) {
										$pubilcation_array[] = [
											'uid' => trim($title[2]),
											'c_id' => $c_id,
											'ref' => (isset($v[0]))?trim(strip_tags($v[0])):'',
											'publication' => (isset($v[1]))?trim(strip_tags($v[1])):'',
											'updated_date' => date('Y-m-d H:i:s'),
											'created_date' => date('Y-m-d H:i:s'),
										];
									}
								}
							}
							// Additionnal payments and statutary duty of accessory payments
							if(strchr($value,'Ca') AND strchr($value,'Additionnal payments'))
							{
								if(!empty($tbody))
								{
									foreach ($tbody as $key => $v) {
										$accessory_payment_array[] = [
											'uid' => trim($title[2]),
											'c_id' => $c_id,
											'in' => (isset($v[0]))?trim(strip_tags($v[0])):'',
											'ca' => (isset($v[1]))?trim(strip_tags($v[1])):'',
											'accessory_payment' => (isset($v[2]))?trim(strip_tags($v[2])):'',
											'updated_date' => date('Y-m-d H:i:s'),
											'created_date' => date('Y-m-d H:i:s'),
										];
									}
								}
							}
							// Branch offices array
							if(strchr($value,'Ca') AND strchr($value,'Branch offices'))
							{
								if(!empty($tbody))
								{
									foreach ($tbody as $key => $v) {
										$branch_office_array[] = [
											'uid' => trim($title[2]),
											'c_id' => $c_id,
											'in' => (isset($v[0]))?trim(strip_tags($v[0])):'',
											'ca' => (isset($v[1]))?trim(strip_tags($v[1])):'',
											'name' => (isset($v[2]))?trim(strip_tags($v[2])):'',
											'updated_date' => date('Y-m-d H:i:s'),
											'created_date' => date('Y-m-d H:i:s'),
										];
									}
								}
							}
							// Journal SOGC
							if(strchr($value,'Journal') AND strchr($value,'Date') AND strchr($value,'SOGC') AND strchr($value,'Page'))
							{
								if(!empty($tbody))
								{
									foreach ($tbody as $key => $v) {
										$id = (isset($v[5]))?trim(strip_tags($v[5])):'';
										preg_match('/\d*/',$id,$page_id);
										$journal_array[] = [
											'uid' => trim($title[2]),
											'c_id' => $c_id,
											'ref' => (isset($v[0]))?trim(strip_tags($v[0])):'',
											'journal' => (isset($v[1]))?trim(strip_tags($v[1])):'',
											'date' => (isset($v[2]))?trim(strip_tags($v[2])):'',
											'sogc' => (isset($v[3]))?trim(strip_tags($v[3])):'',
											'date_sogc' => (isset($v[4]))?trim(strip_tags($v[4])):'',
											'page_id' => trim($page_id[0]),
											'updated_date' => date('Y-m-d H:i:s'),
											'created_date' => date('Y-m-d H:i:s'),
										];
									}
								}
							}
							// Personal Data
							if(strchr($value,'Personal') AND strchr($value,'Function') AND strchr($value,'Signature'))
							{
								if(!empty($tbody))
								{
									foreach ($tbody as $key => $v) {
										$personal_array[] = [
											'uid' => trim($title[2]),
											'c_id' => $c_id,
											'in' => (isset($v[0]))?trim(strip_tags($v[0])):'',
											'mo' => (isset($v[1]))?trim(strip_tags($v[1])):'',
											'ca' => (isset($v[2]))?trim(strip_tags($v[2])):'',
											'personal_data' => (isset($v[3]))?trim(strip_tags($v[3])):'',
											'function' => (isset($v[4]))?trim(strip_tags($v[4])):'',
											'signature' => (isset($v[5]))?trim(strip_tags($v[5])):'',
											'updated_date' => date('Y-m-d H:i:s'),
											'created_date' => date('Y-m-d H:i:s'),
										];
									}
								}
							}
						}

						// echo "<pre>";
						// if(!empty($acts_array)){ print_r($acts_array); }
						// if(!empty($remarks_array)){ print_r($remarks_array); }
						// echo "not empty".(!empty($remarks_array));
						// echo "</pre>";
						
						if(!empty($business_array)) {
							$business_table = $this->db->table('business_name');
							$business_table->insertBatch($business_array);
						}
						if(!empty($purpose_array)) {
							$purpose_table = $this->db->table('purpose');
							$purpose_table->insertBatch($purpose_array);
						}
						if(!empty($capital_array)) {
							$capital_table = $this->db->table('capital');
							$capital_table->insertBatch($capital_array);
						}
						if(!empty($shares_array)) {
							$shares_table = $this->db->table('share_partner');
							$shares_table->insertBatch($shares_array);
						}
						if(!empty($share_capital_array)) {
							$share_capital_table = $this->db->table('share_capital');
							$share_capital_table->insertBatch($share_capital_array);
						}
						if(!empty($com_address_array)) {
							$com_address_table = $this->db->table('company_addresses');
							$com_address_table->insertBatch($com_address_array);
						}
						if(!empty($other_address_array)) {
							$other_address_table = $this->db->table('company_other_addresses');
							$other_address_table->insertBatch($other_address_array);
						}
						if(!empty($remarks_array)) {
							$remarks_table = $this->db->table('remarks');
							$remarks_table->insertBatch($remarks_array);
						}
						if(!empty($acts_array)) {
							$acts_table = $this->db->table('date_of_acts');
							$acts_table->insertBatch($acts_array);
						}
						if(!empty($qualified_array)) {
							$qualified_table = $this->db->table('qualified_facts');
							$qualified_table->insertBatch($qualified_array);
						}
						if(!empty($accessory_payment_array)) {
							$accessory_payment_table = $this->db->table('payments');
							$accessory_payment_table->insertBatch($accessory_payment_array);
							// echo json_encode($accessory_payment_array);
						}
						if(!empty($journal_array)) {
							$journal_table = $this->db->table('journal');
							$journal_table->insertBatch($journal_array);
						}
						if(!empty($personal_array)) {
							$personal_table = $this->db->table('personal_data');
							$personal_table->insertBatch($personal_array);
						}
						if(!empty($pubilcation_array)) {
							$publication_table = $this->db->table('official_publication');
							$publication_table->insertBatch($pubilcation_array);
						}
						if(!empty($branch_office_array)) {
							$branch_table = $this->db->table('branch_office');
							$branch_table->insertBatch($branch_office_array);
						}
						$table->where('id',$res->id)->update(['is_fetched' => 1]);
					} else {
						$table->where('id',$res->id)->update(['is_fetched' => 2]);
					}
				}
				echo "all data inserted...";
			} else {
				echo "The result is not found";
			}
		} else {
			echo "Form is not submitted";
		}
		return redirect()->to(base_url('page-start'));
	}

	//--------------------------------------------------------------------

}
