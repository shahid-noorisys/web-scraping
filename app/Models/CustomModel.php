<?php
namespace App\Models;

use CodeIgniter\Model;

class CustomModel extends Model
{
   protected $builder = null;
   protected $company_info = 'company_info';

   public function getCompanyDetails($data = [])
   {
      if(empty($data)){ return null; }
      $this->builder = $this->db->table($this->company_info);
      return $this->builder->orderBy('id','DESC')->get($data['limit'],$data['offset'])->getResult();
   }

   public function getCompanyDetailsTotal($data = [])
   {
      if(empty($data)){ return null; }
      $this->builder = $this->db->table($this->company_info);
      return $this->builder->countAllResults();
   }
}